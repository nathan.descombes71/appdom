import 'dart:async';

import 'package:app_dom/search_device.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'bluetooth_device_list_entry.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double valueSlider = 0.5;
  final prefs = SharedPreferences.getInstance();

  int milliSecondFumee = 10000;
  double percentFumee = 0;
  bool animateFumee = true;

  int milliSecondAir = 10000;
  double percentAir = 0;
  bool animateAir = true;

  Future<void> initPref() async {
    milliSecondFumee = (await prefs).getInt("milliSecondFumee") ?? 10000;

    milliSecondAir = (await prefs).getInt("milliSecondAir") ?? 10000;

    setState(() {});
  }

  @override
  void initState() {
    initPref();

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (_) => SearchDevice(),
        ),
      );
    });

    super.initState();
  }

  Widget settingBottomSheet(BuildContext ctx) {
    return StatefulBuilder(builder: (context, stateSheet) {
      return Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20, 50, 20, 0),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            const Text(
              "Paramètres",
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            ),
            const Divider(
              thickness: 4,
              color: Colors.black,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text("Retour vibreur"),
                Switch(value: true, onChanged: (val) {}),
              ],
            ),
            const Divider(
              thickness: 4,
              color: Colors.black,
            ),
            InkWell(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (ctx) {
                      return StatefulBuilder(builder: (context, state) {
                        return Dialog(
                          child: IntrinsicHeight(
                            child: Container(
                              padding: const EdgeInsets.all(15),
                              // height: 250,
                              width: 150,
                              child: Column(children: [
                                const Text(
                                  "Délai fumée",
                                  style: TextStyle(fontSize: 20),
                                ),
                                const Divider(
                                  thickness: 3,
                                  color: Colors.black,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text("10s"),
                                    Radio<int>(
                                        value: 10000,
                                        groupValue: milliSecondFumee,
                                        onChanged: (val) async {
                                          milliSecondFumee = val ?? 10000;
                                          (await prefs).setInt(
                                              'milliSecondFumee', val ?? 10000);
                                          stateSheet(() {});
                                          state(() {});
                                        }),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text("15s"),
                                    Radio<int>(
                                        value: 15000,
                                        groupValue: milliSecondFumee,
                                        onChanged: (val) async {
                                          milliSecondFumee = val ?? 15000;
                                          (await prefs).setInt(
                                              'milliSecondFumee', val ?? 15000);
                                          stateSheet(() {});
                                          state(() {});
                                        }),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text("20s"),
                                    Radio<int>(
                                        value: 20000,
                                        groupValue: milliSecondFumee,
                                        onChanged: (val) async {
                                          milliSecondFumee = val ?? 20000;
                                          (await prefs).setInt(
                                              'milliSecondFumee', val ?? 20000);
                                          stateSheet(() {});
                                          state(() {});
                                        }),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text("25s"),
                                    Radio<int>(
                                        value: 25000,
                                        groupValue: milliSecondFumee,
                                        onChanged: (val) async {
                                          milliSecondFumee = val ?? 25000;
                                          (await prefs).setInt(
                                              'milliSecondFumee', val ?? 25000);
                                          stateSheet(() {});
                                          state(() {});
                                        }),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text("30s"),
                                    Radio<int>(
                                        value: 30000,
                                        groupValue: milliSecondFumee,
                                        onChanged: (val) async {
                                          milliSecondFumee = val ?? 30000;
                                          (await prefs).setInt(
                                              'milliSecondFumee', val ?? 30000);
                                          stateSheet(() {});
                                          state(() {});
                                        }),
                                  ],
                                ),
                              ]),
                            ),
                          ),
                        );
                      });
                    });
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 8.0, bottom: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text("Délai fumée"),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Row(children: [
                        Text(
                            (milliSecondFumee / 1000).toStringAsFixed(0) + "s"),
                        const Icon(Icons.chevron_right),
                      ]),
                    )
                  ],
                ),
              ),
            ),
            const Divider(
              thickness: 4,
              color: Colors.black,
            ),
            InkWell(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (ctx) {
                      return StatefulBuilder(builder: (context, state) {
                        return Dialog(
                          child: IntrinsicHeight(
                            child: Container(
                              padding: const EdgeInsets.all(15),
                              // height: 250,
                              width: 150,
                              child: Column(children: [
                                const Text(
                                  "Délai fumée",
                                  style: TextStyle(fontSize: 20),
                                ),
                                const Divider(
                                  thickness: 3,
                                  color: Colors.black,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text("10s"),
                                    Radio<int>(
                                        value: 10000,
                                        groupValue: milliSecondAir,
                                        onChanged: (val) async {
                                          milliSecondAir = val ?? 10000;
                                          (await prefs).setInt(
                                              'milliSecondAir', val ?? 10000);
                                          stateSheet(() {});
                                          state(() {});
                                        }),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text("15s"),
                                    Radio<int>(
                                        value: 15000,
                                        groupValue: milliSecondAir,
                                        onChanged: (val) async {
                                          milliSecondAir = val ?? 15000;
                                          (await prefs).setInt(
                                              'milliSecondAir', val ?? 15000);
                                          stateSheet(() {});
                                          state(() {});
                                        }),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text("20s"),
                                    Radio<int>(
                                        value: 20000,
                                        groupValue: milliSecondAir,
                                        onChanged: (val) async {
                                          milliSecondAir = val ?? 20000;
                                          (await prefs).setInt(
                                              'milliSecondAir', val ?? 20000);
                                          stateSheet(() {});
                                          state(() {});
                                        }),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text("25s"),
                                    Radio<int>(
                                        value: 25000,
                                        groupValue: milliSecondAir,
                                        onChanged: (val) async {
                                          milliSecondAir = val ?? 25000;
                                          (await prefs).setInt(
                                              'milliSecondAir', val ?? 25000);
                                          stateSheet(() {});
                                          state(() {});
                                        }),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text("30s"),
                                    Radio<int>(
                                        value: 30000,
                                        groupValue: milliSecondAir,
                                        onChanged: (val) async {
                                          milliSecondAir = val ?? 30000;
                                          (await prefs).setInt(
                                              'milliSecondAir', val ?? 30000);
                                          stateSheet(() {});
                                          state(() {});
                                        }),
                                  ],
                                ),
                              ]),
                            ),
                          ),
                        );
                      });
                    });
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 8.0, bottom: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text("Délai air"),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Row(children: [
                        Text((milliSecondAir / 1000).toStringAsFixed(0) + "s"),
                        const Icon(Icons.chevron_right),
                      ]),
                    )
                  ],
                ),
              ),
            ),
            const Divider(
              thickness: 4,
              color: Colors.black,
            ),
            InkWell(
              child: Padding(
                padding: const EdgeInsets.only(top: 8.0, bottom: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text("Veille automatique"),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Row(children: const [
                        Text("60s"),
                        Icon(Icons.chevron_right),
                      ]),
                    )
                  ],
                ),
              ),
            ),
            const Divider(
              thickness: 4,
              color: Colors.black,
            ),
            Center(
              child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => SearchDevice(),
                      ),
                    );
                  },
                  child: const Text("Appareils bluetooth")),
            )
          ]),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffeeeeee),
      body: SafeArea(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 30),
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerRight,
                    child: Card(
                      elevation: 7,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50),
                      ),
                      child: InkWell(
                        onTap: () {
                          showModalBottomSheet(
                              isScrollControlled: true,
                              context: context,
                              builder: (ctx) {
                                return settingBottomSheet(ctx);
                              });
                        },
                        child: Container(
                          height: 50,
                          width: 50,
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color.fromARGB(255, 230, 230, 230),
                          ),
                          child: const Icon(
                            Icons.settings_rounded,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 25,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Card(
                            margin: const EdgeInsets.only(bottom: 20),
                            elevation: 7,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(80),
                            ),
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  percentFumee = 1;
                                });

                                Future.delayed(
                                    Duration(
                                        milliseconds: milliSecondFumee + 100),
                                    () {
                                  setState(() {
                                    percentFumee = 0;
                                  });
                                });
                              },
                              child: Container(
                                height: 85,
                                width: 85,
                                decoration: const BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Color.fromARGB(255, 230, 230, 230),
                                ),
                                child: const Center(child: Text("Fumée")),
                              ),
                            ),
                          ),
                          LinearPercentIndicator(
                            animateFromLastPercent: animateFumee,
                            animation: animateFumee,
                            animationDuration: milliSecondFumee,
                            width: 100.0,
                            lineHeight: 8.0,
                            percent: percentFumee,
                            progressColor: const Color(0xff4ecdc4),
                            onAnimationEnd: () {
                              setState(() {
                                animateFumee = false;
                              });

                              Future.delayed(const Duration(milliseconds: 200),
                                  () {
                                setState(() {
                                  animateFumee = true;
                                });
                              });
                            },
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Card(
                            margin: const EdgeInsets.only(bottom: 20),
                            elevation: 7,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(80),
                            ),
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  percentAir = 1;
                                });

                                Future.delayed(
                                    Duration(
                                        milliseconds: milliSecondAir + 100),
                                    () {
                                  setState(() {
                                    percentAir = 0;
                                  });
                                });
                              },
                              child: Container(
                                  height: 85,
                                  width: 85,
                                  decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Color.fromARGB(255, 230, 230, 230),
                                  ),
                                  child: const Center(child: Text("Air"))),
                            ),
                          ),
                          LinearPercentIndicator(
                            animateFromLastPercent: animateAir,
                            animation: animateAir,
                            animationDuration: milliSecondAir,
                            width: 100.0,
                            lineHeight: 8.0,
                            percent: percentAir,
                            progressColor: const Color(0xff4ecdc4),
                            onAnimationEnd: () {
                              setState(() {
                                animateAir = false;
                              });

                              Future.delayed(const Duration(milliseconds: 200),
                                  () {
                                setState(() {
                                  animateAir = true;
                                });
                              });
                            },
                          )
                        ],
                      ),
                    ],
                  ),
                  const Divider(),
                  const Text("Nom de l'artiste" " : " "Nom de la musique"),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Card(
                        elevation: 7,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80),
                        ),
                        child: Container(
                          height: 50,
                          width: 50,
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color.fromARGB(255, 230, 230, 230),
                          ),
                          child: const Center(child: Icon(Icons.volume_down)),
                        ),
                      ),
                      Card(
                        elevation: 7,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80),
                        ),
                        child: Container(
                            height: 50,
                            width: 50,
                            decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                              color: Color.fromARGB(255, 230, 230, 230),
                            ),
                            child:
                                const Center(child: Icon(Icons.skip_previous))),
                      ),
                      Card(
                        elevation: 7,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80),
                        ),
                        child: Container(
                          height: 70,
                          width: 70,
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color.fromARGB(255, 230, 230, 230),
                          ),
                          child: const Center(child: Icon(Icons.play_arrow)),
                        ),
                      ),
                      Card(
                        elevation: 7,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80),
                        ),
                        child: Container(
                            height: 50,
                            width: 50,
                            decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                              color: Color.fromARGB(255, 230, 230, 230),
                            ),
                            child: const Center(child: Icon(Icons.skip_next))),
                      ),
                      Card(
                        elevation: 7,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80),
                        ),
                        child: Container(
                          height: 50,
                          width: 50,
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color.fromARGB(255, 230, 230, 230),
                          ),
                          child: const Center(child: Icon(Icons.volume_up)),
                        ),
                      ),
                    ],
                  ),
                  const Padding(
                    padding: EdgeInsets.only(top: 20, bottom: 10),
                    child: Divider(
                      height: 2,
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Card(
                      elevation: 7,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50),
                      ),
                      child: Container(
                        height: 80,
                        width: 80,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color.fromARGB(255, 230, 230, 230),
                        ),
                        child: const Icon(
                          Icons.unarchive_outlined,
                          size: 35,
                        ),
                      ),
                    ),
                  ),
                  Card(
                    elevation: 7,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(80),
                    ),
                    child: Container(
                      height: 150,
                      width: 150,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color.fromARGB(255, 230, 230, 230),
                      ),
                      child: const Icon(
                        Icons.drive_eta,
                        size: 100,
                      ),
                    ),
                  ),
                  Slider(
                      value: valueSlider,
                      onChanged: (value) {
                        setState(() {
                          valueSlider = value;
                        });
                      }),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: const [
                      Text("Lent"),
                      Text("Rapide"),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
